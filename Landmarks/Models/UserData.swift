//
//  UserData.swift
//  Landmarks
//
//  Created by Dario Scoppelletti on 01/05/2020.
//  Copyright © 2020 Dario Scoppelletti. All rights reserved.
//

import SwiftUI
import Combine

final class UserData: ObservableObject  {
    @Published var showFavoritesOnly = false
    @Published var landmarks = landmarkData
    @Published var profile = Profile.default
}
