//
//  LandmarkList.swift
//  Landmarks
//
//  Created by Dario Scoppelletti on 30/04/2020.
//  Copyright © 2020 Dario Scoppelletti. All rights reserved.
//

import SwiftUI

struct LandmarkList: View {
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        List {
            Toggle(isOn: $userData.showFavoritesOnly) {
                Text("Favorites only")
            }
            
            ForEach(userData.landmarks) { landmark in
                if !self.userData.showFavoritesOnly || landmark.isFavorite {
                    NavigationLink(destination: LandmarkDetail(
                        landmark: landmark)
                        .environmentObject(self.userData)) {
                            LandmarkRow(landmark: landmark)
                    }
                }
            }
        }
        .navigationBarTitle(Text("Landmarks"))
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
        LandmarkList()
            .environmentObject(UserData())
        }
    }
}

