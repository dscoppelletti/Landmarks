Learn to Make Apps with Swift UI
================================

Content: http://developer.apple.com/tutorials/swiftui/tutorials

Follow a series of guided tutorials to learn to make appas using SwiftUI and
Xcode.

## Swift UI Essentials

### Creating and Combining Views

Content: http://developer.apple.com/tutorials/swiftui/creating-and-combining-views

1. Create a New Project and Explore the Canvas
1. Customize the Text View
1. Combine Views Using Stacks
1. Create a Custom Image View
1. Use UIKit and SwiftUI Views Together
1. Compose the Detail View

### Building Lists and Navigation

Content: http://developer.apple.com/tutorials/swiftui/building-lists-and-navigation

1. Get to Know the Sample Data
1. Create the Row View
1. Customize the Row Preview
1. Create the List of Landmarks
1. Make the List Dynamic
1. Set Up Navigation Between List and Detail
1. Pass Data into Child Views
1. Generating Previews Dynamically

### Handling User Input

Content: http://developer.apple.com/tutorials/swiftui/handling-user-input

1. Mark the User's Favorite Landmarks
1. Filter the List View
1. Add a Control to Toggle the State
1. Use an Observable Object for Storage
1. Adopt the Model Object in Your Views
1. Create a Favorite Button For Each Landmark

## Drawing And Animation

### Drawing Paths and Shapes

Content: http://developer.apple.com/tutorials/swiftui/drawing-paths-and-shapes

1. Create a Badge View
1. Draw the Badge Background
1. Draw the Badge Symbol
1. Combine the Badge Foreground and Background

### Animating Views and Transitions

Content: http://developer.apple.com/tutorials/swiftui/animating-views-and-transitions

1. Add Animations to Individual Views
1. Animate the Effects of State Changes
1. Customize View Transitions
1. Compose Animations for Complex Effects

## App Design and Layout

### Composing Complex Interfaces

Content: http://developer.apple.com/tutorials/swiftui/composing-complex-interfaces

1. Add a Home View
1. Create a Category List
1. Add Rows of Landmarks
1. Compose the Home View
1. Add Navigation Between Sections

### Working with UI Controls

Content: http://developer.apple.com/tutorials/swiftui/working-with-ui-controls

1. Display a User Profile
1. Add an Edit Mode
1. Define the Prodile Editor
1. Delay Edit Propagation

## Framework Integration

### Interfacing with UIKit

Content: http://developer.apple.com/tutorials/swiftui/interfacing-with-uikit

1. Create View to Represent a UIPageViewController
1. Create the View Controller's Data Source
1. Track the Page in a SwiftUI View's State
1. Add a Custom Page Control
